function createShdow(ele, x, y, blur, color, offset, count) {
    let shadow = x + "px " + y + "px " + blur + "px " + color + ","
    for (let i = 0; i < count; i++) {
        x = x + offset;
        y = y + offset;
        if (i === count - 1) {
            shadow += x + "px " + y + "px " + blur + "px " + color
        } else {
            shadow += x + "px " + y + "px " + blur + "px " + color + ","
        }
    }
    ele.style.boxShadow = shadow
}
const resume = document.getElementsByClassName('resume-wapper')[0];

createShdow(resume, 2, 2, 0, "#f1f1ff", 4, 8)