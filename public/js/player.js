/**@type{HTMLMediaElement}*/
//格式化时间
function changeTime(time) {
    time = parseInt(time);
    var h = toZero(Math.floor(time / 3600));
    var m = toZero(Math.floor((time % 3600) / 60));
    var s = toZero(Math.round(time % 3600));
    if (h < 1) {
        return m + ":" + s;
    } else {
        return h + ":" + m + ":" + s;
    }
}
//给时间高位补零
function toZero(num) {
    var val = "";
    if (num < 10) {
        val = "0" + num;
    } else {
        val = val + num;
    }
    return val;
}
//改变按钮
function iconChange(obj, oldI, newI) {
    obj.classList.toggle(oldI);
    obj.classList.toggle(newI);
}
/**
 *  左边播放按钮区域
 */

var video = document.querySelector("#my-video video");
(function () {
    function clearInter(intervalId) {
        clearInterval(intervalId);
        intervalId = null;
    }
    //move方法移动进度条背景色
    var callback = {
        moveL: function () {
            clearInterval(intervalId);
            silderBg.style.width = this.offsetLeft + "px";
            video.currentTime = this.offsetLeft / silderBgW;
        },
    };
    /**
     *  规定视频的宽高
     */

    var control = document.querySelector("#my-video .control");
    var vidWapper = document.querySelector("#my-video");
    var intervalId = null;
    video.addEventListener("play", function () {
        video.controls = false;
    });

    /**
     *  进度条
     */
    var playBtn = document.querySelector("#my-video .control .left-btns .play");
    var playIcon = document.querySelector("#my-video .control .left-btns .play i");
    //小滑块
    var silder = document.querySelector("#my-video .silder");
    //获取滑动背景色
    var progress = document.querySelector("#my-video .control > .progress");
    var silderBg = document.querySelector(
        "#my-video .control .progress .inwapper-bg"
    );
    var silderBgW = progress.clientWidth - silder.offsetWidth;
    /*播放时间*/
    var currentTime = document.querySelector("#main #my-video .time .current-time");
    var endTime = document.querySelector("#main #my-video .time .end-time");

    window.onload = function () {
        video.width = vidWapper.clientWidth;
        video.height = vidWapper.clientHeight - control.offsetHeight;
        //在窗口尺寸改变时触发
        window.onresize = function () {
            video.width = vidWapper.clientWidth;
            video.height = vidWapper.clientHeight - control.offsetHeight;
            //在窗口大小改变时，重新获取progress的宽度
            silderBgW = progress.clientWidth - silder.offsetWidth;
        };
    };
    //拖拽
    drag(silder, callback);

    playBtn.onclick = function () {
        //改变图标
        iconChange(playIcon, "icon-bofang", "icon-bofang1");
        //判断视频状态，暂停就播放，播放就暂停
        if (video.paused) {
            video.play();
            intervalId = setInterval(move, 1);
        } else {
            clearInterval(intervalId);
            video.pause();
        }
    };
    video.ondurationchange = function () {
        currentTime.textContent = changeTime(video.currentTime);
        endTime.textContent = changeTime(video.duration);
    };
    function move() {
        silder.style.left = silderBg.style.width =
            (video.currentTime / video.duration) * silderBgW + "px";
    }

    progress.onclick = function (e) {
        clearInterval(intervalId);
        if (video.paused) {
            iconChange(playIcon, "icon-bofang", "icon-bofang1");
        }
        video.play();
        intervalId = setInterval(move, 1);
        video.currentTime =
            video.duration *
            ((e.clientX - this.offsetLeft - control.offsetLeft) / silderBgW);
    };

    /*结束播放按钮*/
    function reset() {
        var resetBtn = document.querySelector("#my-video .control .left-btns .reset");
        resetBtn.addEventListener("click", function () {
            //如果视频是暂停
            if (!video.paused) {
                iconChange(playIcon, "icon-bofang", "icon-bofang1");
            }
            video.load();
        });
    }
    reset();

    /*判断视频是否结束*/
    video.addEventListener("ended", function () {
        clearInterval(intervalId);
        iconChange(playIcon, "icon-bofang", "icon-bofang1");
    });


})();


(function () {
    /**
     * 右边功能区
     */
    var callback = {
        moveT: function () {
            volSilderBg.style.height =
                this.parentNode.clientHeight - this.offsetTop + "px";
            volume =
                (this.parentNode.offsetHeight - this.offsetTop) /
                (this.parentNode.offsetHeight + this.offsetHeight);
            video.volume =
                (this.parentNode.offsetHeight - this.offsetTop) /
                (this.parentNode.offsetHeight + this.offsetHeight);
            volText.textContent = Math.floor(video.volume * 100);
            newVolSilderBg = volSilderBg.style.height;
            newVolSilder = volSilder.style.top;
        },
    };

    /*音量功能键*/
    var volSilder = document.querySelector(
        "#my-video .control .right-btns .sound .progress .contain .inwapper .silder"
    );
    var volSilderBg = document.querySelector(
        "#my-video .control .right-btns .sound .progress .contain .inwapper-bg"
    );
    var soundI = document.querySelector("#my-video .sound .btn i");
    var volText = document.querySelector("#my-video .sound .progress .volume");
    var newVolSilderBg = 0;
    var newVolSilder = 0;
    soundI.addEventListener("click", function () {
        //改变图标
        iconChange(soundI, "icon-yinliang", "icon-jingyin");

        //判断当前是否禁音
        if (video.muted) {
            video.muted = false;
            volText.textContent = Math.floor(video.volume * 100);
            volSilder.style.top = newVolSilder;
            volSilderBg.style.height = newVolSilderBg;
        } else {
            volSilderBg.style.height = 0;
            volSilder.style.top = volSilder.parentNode.clientHeight + "px";
            volText.textContent = 0;
            video.muted = true;
        }
    });
    drag(volSilder, callback);

    /**
     * 全屏功能区
     */
    var fullScreenBtn = document.querySelector("#main #my-video .full-screen")
    fullScreenBtn.addEventListener('click', function () {
        if (IsFull()) {
            exitFull()
            return
        }
        requestFullScreen(video)
    })
})();